# Description #

This contact list sample application is an starter app for understanding MEAN stack.

### What is this repository contains? ###

* 1. Nodejs, ExpressJS, Angular4 and MongoDB
* Version 1.0.0

### How do I get set up? ###

* Install node
* Install mongodb community edition and start mongodb server.
* Run - npm install
* Run - angular4 client app inside client folder using npm start which will run in port 4200 and express app from contactlist folder which will run in port 3000
* Now go to localhost://4200
* Make sure cors is added into your express app. It is mandatory to communicate between different client and server app in unidentical ports 4200 and 3000
