const express = require("express");
const router = express.Router();

const Contact = require("../models/contacts");

//retriving contact
router.get("/contacts", function(req,res, next){
    Contact.find(function(err, conatcts){
        res.json(conatcts);
    })
});

// add contact
router.post("/contact", function(req,res, next){
    let newContact = new Contact({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        phone: req.body.phone
    });
    newContact.save(function(err, contact){
        if(err){
            res.json({msg: "failed to add"});
        }
        else{
            res.json({msg: "contact added successfully"});
        }
    });
});

//delete contact
router.delete("/contact/:id", function(req,res, next){
    Contact.remove({_id: req.params.id}, function(err, result){
        if(err){
            res.json(err);
        }
        else{
            res.json(result);
        }
    });
});

//update contact
router.put("/contact/:id", function(req, res, next){
    Contact.updateOne({_id: req.params.id},req.body, function(err, result){
        if(err){
            res.json(err);
        }
        else{
            console.log(result);
            res.json(result);
        }
    });
});
module.exports = router;