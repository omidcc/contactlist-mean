import { Injectable } from '@angular/core';
import {Http, Headers} from "@angular/http";
import {Contact} from "./Contact";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ContactService {

  constructor(private http: Http) { }

  //retrive Contacts 
  getContacts(){
    return this.http.get("http://localhost:3000/api/contacts")
      .toPromise()
      .then(response => response.json() as Contact[])
  }

  //a a new contact
  addContact(newContact){
    var headers = new Headers();
    headers.append("Content-Type","application/json");

    return this.http.post("http://localhost:3000/api/contact", newContact,{headers: headers})
            .toPromise()
            .then(response => response.json() as Contact);
  }
  // delete Contact
  deleteContact(id){
    return this.http.delete("http://localhost:3000/api/contact/"+id)
          .toPromise()
          .then(response => response.json() as String)
  }

  //update contact
  updateContact(updatedContact){
    var headers = new Headers();
    headers.append("Content-Type","application/json");
    return this.http.put("http://localhost:3000/api/contact/"+updatedContact.id, updatedContact,{headers: headers})
            .toPromise()
            .then(response => response.json() as Contact);
  }
}
