import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { Contact } from '../contact';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
  providers:[ContactService]
})
export class ContactsComponent implements OnInit {

  contacts: Contact[]
  first_name: string;
  last_name: string;
  phone: string;
  id: string;
  isAddBtnVisible: boolean=true;
  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.loadContacts();
  }
  loadContacts(){
    this.contactService
      .getContacts()
      .then((contacts: Contact[]) => {
        this.contacts = contacts.map((contact) => {
          return contact;
        });
      });
  }
  addContact(){
    const newContact = {
      first_name: this.first_name,
      last_name: this.last_name,
      phone: this.phone
    }
    this.contactService.addContact(newContact)
        .then((contact)=>{
          this.contacts.push(contact);
          this.loadContacts();
          this.first_name = this.last_name = this.phone ="";
        });
  }

  deleteContact(id: any){
    var contacts = this.contacts;
    this.contactService.deleteContact(id)
    .then((res)=>{
      if(res.n==1){
          for(var i=0; i<contacts.length; i++){
            if(contacts[i]._id==id){
              contacts.splice(i,1);
            }
          }
      }
    });
  }
  editContact(contact: Contact){
    this.isAddBtnVisible=false;
    this.first_name = contact.first_name;
    this.last_name = contact.last_name;
    this.phone = contact.phone;
    this.id = contact._id;
  }

  updateSelected(){
    const updatedContact = {
      _id: this.id,
      first_name: this.first_name,
      last_name: this.last_name,
      phone: this.phone
    }
    this.contactService.updateContact(updatedContact)
        .then((contact)=>{
          this.loadContacts();
          this.clearForm();
        });
  }
  clearForm(){
    this.isAddBtnVisible=true;
    this.first_name = this.last_name = this.phone ="";
  }
}
