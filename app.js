//import modules
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var cors = require("cors");
var path = require("path");//nodejs core module, don't need to install separately


var app = express();

//connect to mongodb
mongoose.connect("mongodb://localhost:27017/contactlist");
//on connection
mongoose.connection.on("connected", function(){
    console.log("connected to database");
});
mongoose.connection.on("error", function(err){
    if(err){
        console.log("database error: " + err);
    }
});
//adding middleware
app.use(cors());
app.use(bodyParser.json());

//static files
app.use(express.static(path.join(__dirname,"public")));
//add routes
const route = require("./routes/route");
app.use("/api",route);

//testing server
app.get("/", function(req,res){
    res.send("success");
});
// port
const port = 3000;

app.listen(port, function(){
    console.log("server started at port:" +port);
});